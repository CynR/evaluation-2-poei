package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;
    private RawgDatabaseClient rawgDatabaseClient;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository, RawgDatabaseClient rawgDatabaseClient) {
        this.videoGamesRepository = videoGamesRepository;
        this.rawgDatabaseClient = rawgDatabaseClient;
    }

    public List<VideoGame> ownedVideoGames() {
        List<VideoGame> listVideoGames = new ArrayList<>();
        List<Optional<VideoGame>> listOptionalVideoG = this.videoGamesRepository.getAll();
        for(long x = 0; x < listOptionalVideoG.size(); x++) {
            listVideoGames.add(this.videoGamesRepository.get(x).get());
        }
        return listVideoGames;
    }

    public VideoGame getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id).get();
    }

    public VideoGame addVideoGame(String name) {
        VideoGame newGame = rawgDatabaseClient.getVideoGameFromName(name).get();
        videoGamesRepository.save(newGame);
        return (newGame);
    }
}
