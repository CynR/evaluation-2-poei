package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesRepository {

    private HashMap<Long, Optional<VideoGame>> videoGamesById = new HashMap<>();

    public List<Optional<VideoGame>> getAll() {
        return List.copyOf(this.videoGamesById.values());
    }

    public Optional<VideoGame> get(Long id) {
        return this.videoGamesById.get(id);
    }

    public void save(VideoGame v) {
        this.videoGamesById.put(v.getId(), Optional.of(v));
    }



}
