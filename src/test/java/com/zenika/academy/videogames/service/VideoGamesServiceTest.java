package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class VideoGamesServiceTest {

    @Mock
    private RawgDatabaseClient client;

    @InjectMocks
    private VideoGamesService videoGamesService;

    @Test
    void doesTheVideoIsAdded(){
        VideoGamesService vGS = this.videoGamesService;

        HashMap<Long, Optional<VideoGame>> videoGamesById = new HashMap<>();
        videoGamesById.put((long) 1, (this.client.getVideoGameFromName("Super Mario Kart")));
        when(this.client.getVideoGameFromName("Super Mario Kart")).thenReturn
                ((videoGamesById.get((long) 1)));
       // Assertions.assertEquals(this.client.getVideoGameFromName("Super Mario Kart"),
       //         vGS.addVideoGame("Super Mario Kart"));
        assertFalse(this.client.getVideoGameFromName("Super Mario Kart").isPresent());


    }


}
